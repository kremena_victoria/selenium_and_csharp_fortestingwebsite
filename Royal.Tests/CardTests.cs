using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Tests
{
    public class CardTests
    {
        IWebDriver driver;
        [SetUp]
        public void BeforeEah()
        {
            driver = new ChromeDriver(Path.GetFullPath(@"../../../../"+ "_driver"));
        }

        [TearDown]
        public void AfterEach()
        {
            driver.Quit();
        }
        [Test]
        public void Ice_Spirit_is_on_Cards_page()
        {
            // 1.Go to https://statsroyale.com
            driver.Url = "https://statsroyale.com";

            // 2. Click on the Cards link in the header nav
            driver.FindElement(By.CssSelector("a[href='/cards']")).Click();

            // 3. Assert Ice spirit is displayed
            var IceSpirit = driver.FindElement(By.CssSelector("a[href='Ice+Spirit']"));
            Assert.That(IceSpirit.Displayed);
        }

         [Test]
        public void Ice_Spirit_headers_are_correct_on_Card_Detail_page()
        {
            // 1.Go to https://statsroyale.com
            driver.Url = "https://statsroyale.com";

            // 2. Click on the Cards link in the header nav
            driver.FindElement(By.CssSelector("a[href='/cards']")).Click();

            // 3. Go to Ice spirit details page
            driver.FindElement(By.CssSelector("a[href='Ice+Spirit']")).Click();
            
            //4. Assert basic header stats
            
        }
    }
}